require 'test_helper'

class LikeTest < ActiveSupport::TestCase
  setup do
    @taro = User.create(email: "taro@example.com", password: "12345678", username: "taro")
    @hanako = User.create(email: "hanako@example.com", password: "12345678", username: "hanako")
    @sunny = @taro.tweets.create(content: "今日は良い天気")
    @rain = @hanako.tweets.create(content: "明日は雨らしい")
  end

  test "Likeできる" do
    @sunny.like(@taro.id)

    assert Like.exists?(tweet: @sunny, user: @taro)
  end

  test "Unlikeできる" do
    @sunny.like(@taro.id)
    @sunny.unlike(@taro.id)

    assert_not Like.exists?(tweet: @sunny, user: @taro)
  end

  test "ユーザーがLikeしていたらtrueを返す" do
    @sunny.like(@taro.id)

    assert @sunny.liked?(@taro)
  end

  test "ユーザーがLikeしていなかったらfalseを返す" do
    @sunny.like(@taro.id)
    
    assert_not @sunny.liked?(@hanako)
  end

  test "ツイートのLike数が正しい" do
    @sunny.like(@taro.id)
    @sunny.like(@hanako.id)

    assert @sunny.liked_count == 2
  end

  test "ユーザーのLike数が正しい" do
    @sunny.like(@taro.id)
    @rain.like(@taro.id)

    assert @taro.liked_count == 2
  end

  test "ツイートにLikeしているユーザーのリストが取得できる" do
    @sunny.like(@taro.id)
    @sunny.like(@hanako.id)
    users = @sunny.liked_users

    assert users.size == 2
    assert_includes users, @taro
    assert_includes users, @hanako
  end

  test "ユーザーがLikeしているツイートのリストが取得できる" do
    @sunny.like(@taro.id)
    @rain.like(@taro.id)
    tweets = @taro.liked_tweets

    assert tweets.size == 2
    assert_includes tweets, @sunny
    assert_includes tweets, @rain
  end

end