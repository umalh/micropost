class LikesController < ApplicationController
  def index
    @user = User.find(params[:user_id])
    @tweets = @user.liked_tweets.order(created_at: :desc)

    render "users/show"
  end

  def create
    if(user_signed_in?)
      tweet = Tweet.find(params[:tweet_id])
      if tweet.liked?(current_user.id) == false
        tweet.like(current_user.id)
      end

      redirect_back(fallback_location: root_path)
    else
      redirect_to "/users/sign_in"
    end
  end

  def destroy
    if(user_signed_in?)
      tweet = Tweet.find(params[:tweet_id])
      tweet.unlike(current_user.id)

      redirect_back(fallback_location: root_path)
    else
      redirect_to "/users/sign_in"
    end
  end
end
