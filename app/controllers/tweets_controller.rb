class TweetsController < ApplicationController
  def index
    @tweet = Tweet.new
    @tweets = Tweet.all.order(created_at: :desc)
  end

  def create
    if(user_signed_in?)
      @tweet = current_user.tweets.build(tweet_params)
      @tweet.save
      redirect_to tweets_path
    else
      redirect_to "/users/sign_in"
    end
  end

  def destroy
    if(user_signed_in?)
      @tweet = Tweet.find(params[:id])
      @tweet.destroy
      redirect_to tweets_path
    else
      redirect_to "/users/sign_in"
    end
  end

  private
    def tweet_params
      params.require(:tweet).permit(:content)
    end
end
