class StaticPagesController < ApplicationController
  def home
    redirect_to tweets_path if user_signed_in?
  end
end
