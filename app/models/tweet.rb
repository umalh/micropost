class Tweet < ApplicationRecord
  belongs_to :user
  validates :content, {presence: true}
  has_many :likes, :dependent => :destroy
  has_many :liked_users, :through => :likes, :source => :user

  def like(user_id)
    self.likes.create(user_id: user_id)
  end

  def unlike(user_id)
    self.likes.where(user_id: user_id).destroy_all
  end

  def liked?(user_id)
    self.likes.exists?(user_id: user_id)
  end

  def liked_count
    likes.size
  end
end
