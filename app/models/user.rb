class User < ApplicationRecord
  validates :username, {presence: true, uniqueness: true, length: {maximum: 20}}
  has_many :tweets 
  has_many :likes
  has_many :liked_tweets, :through => :likes, :source => :tweet
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  def liked_count
    likes.size
  end
end
