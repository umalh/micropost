Rails.application.routes.draw do

  root 'static_pages#home'
  devise_for :users

  resources :tweets do
    post 'like' => 'likes#create'
    delete 'like' => 'likes#destroy'
  end
    
  resources :users, only: [:show] do
    get 'likes' => 'likes#index'
  end

  get 'likes/create'

  get 'likes/destroy'
end
